FROM  golang:alpine AS build

WORKDIR /src/

COPY *.go .

COPY go.mod .

RUN go mod download

RUN go build -o  /tmp/demo

FROM alpine

COPY --from=build /tmp/demo /tmp/demo

ENTRYPOINT ["/tmp/demo"]